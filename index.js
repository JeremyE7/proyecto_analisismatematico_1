document.addEventListener('DOMContentLoaded', function()
{
    if(localStorage.getItem("puntaje") != null) 
    {
        cargarPuntaje();
    }

    const puerta1 = crearPuerta(document.getElementById("p1"));
    const puerta2 = crearPuerta(document.getElementById("p2"));
    const puerta3 = crearPuerta(document.getElementById("p3"));
    const puertasT = [puerta1,puerta2,puerta3]

    const btn_nJuego = document.getElementById("btn_nJuego");
    btn_nJuego.onclick = function()
    {
        location.reload();
    }; 

    generarContenido(puertasT)
    console.log(puertasT[0].contenido);
    console.log(puertasT[1].contenido);
    console.log(puertasT[2].contenido);
    let contador = 0;
    seleccionarPuerta(puertasT, contador)
})

function generarContenido(puertas)
{
    const premios = ["cabra", "carro"];
    let maximo = premios.length;
    let minimo = 0;
    let contCeros = 0;
    

    puertas.forEach(e => 
    {
        let azar = generarNumeroAleatorio(maximo, minimo)
        if (azar == 1)
        {
            maximo = 1;
            minimo = 0;
        }
        else
        {
            contCeros++;
            if(contCeros == 2)
            {
                maximo = 1
                minimo = 1;
            }
        }

        e.contenido = premios[azar];    

    });
}

//maximo es el valor que va a indicar el limite del Math.Random
//sumador sirve para indicar el minimo del Math.Random
function generarNumeroAleatorio(maximo, minimo)
{
    return Math.floor(Math.random()*maximo) + minimo;
}

function crearPuerta(e)
{
    p =
    {
        puerta: e,
        contenido: ""
    };
    
    return p;
}

function seleccionarPuerta(puertasT, c)
{
    let numeroValido = false;
    puertaSeleccionada = null;
    cambio = false;

    puertasT.forEach(e => 
    {
        e.puerta.onclick = function()
        {
            c++;
            if(c == 1)
            {
                while(numeroValido==false)
                {
                    n = generarNumeroAleatorio(3,0);
                    if(n != puertasT.indexOf(e) && puertasT[n].contenido == "cabra") numeroValido = true;
                }

                str = "img" + (n + 1)

                var img = document.getElementById(str);
                img.src = "https://www.petdarling.com/wp-content/uploads/2021/03/cabras.jpg"
                console.log("Desea cambiar de puerta?")
                puertaSeleccionada = e;
                        
            }
            else
            {
                if(puertaSeleccionada == e)
                    cambio = false;
                else
                    cambio = true;
                
                str = "img" + (puertasT.indexOf(e) + 1);
                var img = document.getElementById(str);

                if(e.contenido == "cabra")
                {
                    img.src = "https://www.petdarling.com/wp-content/uploads/2021/03/cabras.jpg";

                }
                else
                {
                    img.src = "https://www.elcarrocolombiano.com/wp-content/uploads/2021/02/20210208-TOP-75-CARROS-MAS-VENDIDOS-DE-COLOMBIA-EN-ENERO-2021-01.jpg";

                }
                guardarPuntaje(e,cambio)
                desactivarBotones(puertasT);
            }
        }    
    });
}

function guardarPuntaje(e, cambio)
{
    const tb_perd_camb = document.getElementById("perd_camb");
    const tb_perd_no_camb = document.getElementById("perd_no_camb");
    const tb_vict_camb = document.getElementById("vict_camb");
    const tb_vict_no_camb = document.getElementById("vict_no_camb");
    let puntaje = [0,0,0,0]

    if(localStorage.getItem("puntaje") != null) 
    {
        puntaje = localStorage.getItem("puntaje").split(",");
        cargarPuntaje();
    }
        
    

    if(e.contenido == "cabra")
    {
        if(cambio == true)
        {
            tb_perd_camb.innerHTML = parseInt(tb_perd_camb.innerHTML) + 1
            puntaje[2]++;
        }
        else
        {
            tb_perd_no_camb.innerHTML = parseInt(tb_perd_no_camb.innerHTML) + 1
            puntaje[3]++;
        }
    }
    else
    {
        if(cambio == true)
        {
            tb_vict_camb.innerHTML = parseInt(tb_vict_camb.innerHTML) + 1
            puntaje[0]++;
        }
        else
        {
            tb_vict_no_camb.innerHTML = parseInt(tb_vict_no_camb.innerHTML) + 1
            puntaje[1]++;
        }
        
    }

    localStorage.setItem("puntaje",puntaje);
    cargarPuntaje()
}


function cargarPuntaje()
{
    let puntaje = localStorage.getItem("puntaje").split(",");

    const tb_perd_camb = document.getElementById("perd_camb");
    const tb_perd_no_camb = document.getElementById("perd_no_camb");
    const tb_vict_camb = document.getElementById("vict_camb");
    const tb_vict_no_camb = document.getElementById("vict_no_camb");

    tb_perd_no_camb.innerHTML = puntaje[3];
    tb_perd_camb.innerHTML = puntaje[2];
    tb_vict_no_camb.innerHTML = puntaje[1];
    tb_vict_camb.innerHTML = puntaje[0];

}

function desactivarBotones(botones)
{
    botones.forEach(element => {
        element.puerta.disabled = true;
    });
}